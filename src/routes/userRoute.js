const express = require("express");
const userRoute = express.Router();
// import userController
const userController = require("../controllers/userController");
userRoute.get("/likereslist", userController.getLikeResList);
userRoute.post("/addlikeres", userController.likeRest);
userRoute.delete("/unlikeres", userController.unlikeRest);
userRoute.get("/listrate", userController.getRateResList);
userRoute.post("/addrateres", userController.addResRate);
module.exports = userRoute;
